require 'compass-normalize'
require 'compass/import-once/activate'
require 'rgbapng'
# Require any additional compass plugins here.


# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "/css"
sass_dir = "assets/scss"
images_dir = "assets/img"
http_images_dir = "assets/img"
generated_images_dir = "assets/img/generated_images"
fonts_dir = "assets/fonts"

add_import_path "base"
add_import_path "helpers"
add_import_path "layout"
add_import_path "libs"
add_import_path "mixins"
add_import_path "partials"


enviroment = :development

# You can select your preferred output style here (can be overridden via the command line):
output_style = :nested

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# Copies the style.css from /css to the root folder when compiling is finished

# require 'fileutils'
# on_stylesheet_saved do |file|
#   if File.exists?(file) && File.basename(file) == "style.css"
#     puts "Moving: #{file}"
#     FileUtils.cp(file, File.dirname(file) + "/../" + File.basename(file))
#   end
# end

$(document).ready(function () {
	navigationShow ('b__navigation');
	navigationShow ('b__navigation--vertical');
	parallax ();
	// landingElements();
	if ($('#exampleGridParent').length)
		animateElement ('#exampleGridParent', '.row div[class*=col]', 'zoom', 'exponential');
	// $('#exampleGridParent').elemAnimate('.row div[class*=col]', {
	// 	animation: 'right',
	// 	delay: 500,
	// 	easing: 'exponential'
	// });
});


function navigationShow (navigation) {
	// var $navigation = $('.b__navigation'),
	var $navigation = $('.' + navigation),
		$subLevelTrigger = $navigation.find('li').has('ul'),
		$subLevel = $subLevelTrigger.children('ul');

	// Hover on element that has sublevel - show sublevel
	$subLevelTrigger.hover(function () {
		var $this = $(this);

		// Hover on sublevel with sub-sub level - show its sublevel
		$this.children('ul').slideDown().hover(function () {
			var $self = $(this);

			$self.children('ul').slideDown();
			},
			function () {
				var $self = $(this);

				$self.children('ul').stop().slideUp();
			});
			// #Hover on sublevel
		},
		function () {
			var $this = $(this);

			$this.children('ul').stop().slideUp();
		});
		// #Hover on first level with sublevel
}

function parallax () {
	var conHeight = $('.bird-box').height();

	$(window).on('scroll', function () {
		var wScroll = $(this).scrollTop();

		if (wScroll <= conHeight) {
			$('.logo').css({
	            'transform' : 'translate(0px, '+ wScroll /2 +'%)'
	        });

	        $('.back-bird').css({
	            'transform' : 'translate(0px, '+ wScroll /4 +'%)'
	        });

	        $('.fore-bird').css({
	            'transform' : 'translate(0px, -'+ wScroll /40 +'%)'
	        });
		}
	});
}

/*
* Animate selected elements
*/
function animateElement (parent, elements, animation, linear, delay) {

	var $parent = $(parent),
		$elements = $parent.find(elements),
		$window = $(window);

	if (typeof animation === 'undefined' || animation === null)
		animation = 'left';

	if (typeof linear === 'undefined' || linear === null)
		linear = 'linear';

	if (typeof delay === 'undefined' || delay === null)
		delay = 700;

	$elements.addClass('animate-' + animation);


	$window.on('scroll', function () {
		var wScroll = $(this).scrollTop();

		// Check if it is time for the elements to show, i.e. if the user has scrolled to the parent div
		if (wScroll > $parent.offset().top - $window.height() / 1.5) {

            if (linear == 'exponential') {
				$elements.each(function (index) {
					setTimeout( function () {
						$elements.eq(index).removeClass('animate-' + animation).addClass('is-showing');
					}, delay * (Math.exp(index * 0.10)) - delay);
				});
			}
			else {
				$elements.each( function (index) {
					setTimeout( function () {
						$elements.eq(index).removeClass('animate-' + animation).addClass('is-showing');
					}, delay * (index * 0.10) - delay);
				});
			}
		}
	});
}

function periscopeMove () {
	// var $element = $()
}

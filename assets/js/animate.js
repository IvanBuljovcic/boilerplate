(function ($) {

    $.fn.elemAnimate = function (selector, options) {

        options = options || {};
        var selector = $(selector);

        var opts = $.extend( {}, defaults, options);

        var defaults = {
            animation: 'left',
            easing: 'linear',
            delay: 700
        };

        var defAnimate = defaults.animation,
            defDelay = defaults.delay;

        if (typeof options === 'undefined') {
            selector.addClass('animate-' + defAnimate);

            selector.each( function (index) {
                setTimeout( function () {
                    selector.eq(index).removeClass('animate-' + defAnimate).addClass('is-showing');
                }, defDelay * (index * 0.10) - defDelay);
            });
        }

        else {
            var optAnimate = options.animation,
                optDelay = options.delay;

            selector.addClass('animate-' + optAnimate);

            if (typeof options.easing === 'undefined' || options.easing === 'linear') {
                selector.each( function (index) {
                    setTimeout( function () {
                        selector.eq(index).removeClass('animate-' + optAnimate).addClass('is-showing');
                    }, optDelay * (index * 0.10) - optDelay);
                });
            }
            else{
                selector.each( function (index) {
                    setTimeout ( function () {
                        selector.eq(index).removeClass('animate-' + optAnimate).addClass('is-showing');
                    }, optDelay * (Math.exp(index * 0.10)) - optDelay);
                });
            }
        }
    };

    $.fn.elemAnimate.defaults = {
        animation: 'left',
        linear: 'linear',
        delay: 500
    };

})(jQuery);
